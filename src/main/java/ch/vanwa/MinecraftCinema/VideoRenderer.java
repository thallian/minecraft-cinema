package ch.vanwa.MinecraftCinema;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import org.bukkit.entity.Player;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapView;

public final class VideoRenderer extends MapRenderer {

    private BufferedImage image = null;

    public VideoRenderer() {

    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    @Override
    public void render(MapView mapView, MapCanvas mapCanvas, Player player) {
        if (this.image != null) {
            mapCanvas.drawImage(0, 0, this.image);
        }
    }
}
