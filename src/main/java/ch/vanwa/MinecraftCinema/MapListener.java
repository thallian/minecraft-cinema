package ch.vanwa.MinecraftCinema;

import java.util.logging.Logger;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.server.MapInitializeEvent;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

public final class MapListener implements Listener {

    protected static final Logger logger = Logger.getLogger("Minecraft.MapListener");

    @EventHandler
    public void onMap(MapInitializeEvent e) {
        MapView mapView = e.getMap();

        for (MapRenderer renderer : mapView.getRenderers()) {
            mapView.removeRenderer(renderer);
        }

        VideoRenderer renderer = new VideoRenderer();

        CinemaPlugin.plugin.videoPlayer.register(renderer);
        mapView.addRenderer(renderer);
    }
}
