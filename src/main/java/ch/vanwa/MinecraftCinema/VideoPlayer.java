package ch.vanwa.MinecraftCinema;

import java.util.logging.Logger;
import java.util.ArrayList;
import java.io.File;
import java.io.FilenameFilter;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.RenderCallback;
import uk.co.caprica.vlcj.player.direct.RenderCallbackAdapter;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;

public final class VideoPlayer {

    protected static final Logger logger = Logger.getLogger("Minecraft.VideoPlayer");

    private final ArrayList<String> playlist = new ArrayList<String>();
    private int playlistPosition = 0;

    private static final int width = 125;
    private static final int height = 125;

    private final BufferedImage image;
    private final DirectMediaPlayerComponent mediaPlayerComponent;

    private final String[] mediaOptions;

    private ArrayList<VideoRenderer> renderers;

    public VideoPlayer(int audioPort) {
        boolean found = new NativeDiscovery().discover();

        BufferFormatCallback bufferFormatCallback = new BufferFormatCallback() {
            @Override
            public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
                return new RV32BufferFormat(width, height);
            }
        };

        this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        this.mediaPlayerComponent = new DirectMediaPlayerComponent(bufferFormatCallback) {
            @Override
            protected RenderCallback onGetRenderCallback() {
                return new VideoRenderCallbackAdapter();
            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {
                next();
            }
        };

        this.mediaPlayerComponent.getMediaPlayer().setPlaySubItems(true);

        String vlcOptions = String.format("sout=#duplicate{dst=display,select=video,dst=es{access-audio=http,mux-audio=ogg,dst-audio=localhost:%s},select=audio}", audioPort);
        this.mediaOptions = new String[] { vlcOptions };

        initializePlaylist();
        play(0);

        this.renderers = new ArrayList<VideoRenderer>();
    }

    public void initializePlaylist() {
        this.playlist.clear();

        File videoDir = new File(CinemaPlugin.plugin.getDataFolder(), "videos");
        File[] videos = videoDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".webm");
            }
        });

        for (File video : videos) {
            this.playlist.add(video.getAbsolutePath());
        }
    }

    public void play() {
        this.mediaPlayerComponent.getMediaPlayer().play();
    }

    public void play(int position) {
        if (this.playlist.size() > 0) {
            String videoPath = this.playlist.get(position);
            this.mediaPlayerComponent.getMediaPlayer().playMedia(videoPath, this.mediaOptions);
            this.playlistPosition = position;
        }
    }

    public void pause() {
        this.mediaPlayerComponent.getMediaPlayer().pause();
    }

    public void next() {
        if (this.playlist.size() > 0) {
            if (this.playlistPosition == this.playlist.size() - 1) {
                this.playlistPosition = -1;
            }
            play(++this.playlistPosition);
        }
    }

    public void previous() {
        if (this.playlist.size() > 0) {
            if (this.playlistPosition == 0) {
                this.playlistPosition = this.playlist.size();
            }
            play(--this.playlistPosition);
        }
    }

    public String[] list() {
        if (this.playlist.size() > 0) {
            String[] playlistArray = new String[playlist.size()];
            playlistArray = this.playlist.toArray(playlistArray);

            return playlistArray;
        }

        return new String[] {};
    }

    public void register(VideoRenderer renderer) {
        this.renderers.add(renderer);
        renderer.setImage(this.image);
    }

    private class VideoRenderCallbackAdapter extends RenderCallbackAdapter {

        private VideoRenderCallbackAdapter() {
            super(new int[width * height]);
        }

        @Override
        protected void onDisplay(DirectMediaPlayer mediaPlayer, int[] rgbBuffer) {
            image.setRGB(0, 0, width, height, rgbBuffer, 0, width);
        }
    }
}
