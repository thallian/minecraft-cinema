package ch.vanwa.MinecraftCinema;

import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.bukkit.event.Event;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import org.bukkit.plugin.PluginManager;

public final class CinemaPlugin extends JavaPlugin {

    protected static final Logger logger = Logger.getLogger("Minecraft.CinemaPluginPlugin");
    public static CinemaPlugin plugin;

    public VideoPlayer videoPlayer;

    @Override
    public void onEnable() {
        logger.info(String.format(
                        "%s %s enabled",
                        getDescription().getName(),
                        getDescription().getVersion()));
        plugin = this;

        this.getDataFolder().mkdirs();

        this.saveDefaultConfig();
        registerEvents();
        registerCommands();

        int audioPort = this.getConfig().getInt("audio_port");
        this.videoPlayer = new VideoPlayer(audioPort);

        short id = (short)this.getConfig().getInt("map_id");
        MapView mapView = Bukkit.getMap(id);
        if (mapView == null) {
            mapView = Bukkit.createMap(Bukkit.getWorlds().get(0));
            this.getConfig().set("map_id", mapView.getId());
        }

        for (MapRenderer renderer : mapView.getRenderers()) {
            mapView.removeRenderer(renderer);
        }

        VideoRenderer renderer = new VideoRenderer();

        CinemaPlugin.plugin.videoPlayer.register(renderer);
        mapView.addRenderer(renderer);
    }

    @Override
    public void onDisable() {
        logger.info(String.format(
                        "%s %s disabled",
                        getDescription().getName(),
                        getDescription().getVersion()));
        plugin = null;
    }

    protected void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new MapListener(), this);
    }

    protected void registerCommands() {
        CinemaCommandExecutor commandExecutor = new CinemaCommandExecutor();

        this.getCommand("play").setExecutor(commandExecutor);
        this.getCommand("pause").setExecutor(commandExecutor);
        this.getCommand("next").setExecutor(commandExecutor);
        this.getCommand("prev").setExecutor(commandExecutor);
        this.getCommand("playlist").setExecutor(commandExecutor);
    }
}
