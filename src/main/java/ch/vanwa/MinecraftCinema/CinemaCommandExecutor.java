package ch.vanwa.MinecraftCinema;

import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.bukkit.event.Event;
import java.util.logging.Logger;
import org.bukkit.entity.Player;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;

public class CinemaCommandExecutor implements CommandExecutor {

    protected static final Logger logger = Logger.getLogger("Minecraft.CinemaCommandExecutor");

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("play")) {
            if(checkPermission(sender, "cinema.play")) {
                if (args.length > 0) {
                    int playlistPosition = Integer.parseInt(args[0]);
                    CinemaPlugin.plugin.videoPlayer.play(playlistPosition);
                } else {
                    CinemaPlugin.plugin.videoPlayer.play();
                }

                return true;
            }
        } else if (cmd.getName().equalsIgnoreCase("pause")) {
            if(checkPermission(sender, "cinema.pause")) {
                CinemaPlugin.plugin.videoPlayer.pause();
                return true;
            }
        } else if (cmd.getName().equalsIgnoreCase("next")) {
            if(checkPermission(sender, "cinema.next")) {
                CinemaPlugin.plugin.videoPlayer.next();
                return true;
            }
        } else if (cmd.getName().equalsIgnoreCase("prev")) {
            if(checkPermission(sender, "cinema.prev")) {
                CinemaPlugin.plugin.videoPlayer.previous();
                return true;
            }
        } else if (cmd.getName().equalsIgnoreCase("playlist")) {
            if(checkPermission(sender, "cinema.playlist")) {
                String[] playlist = CinemaPlugin.plugin.videoPlayer.list();

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < playlist.length; i++) {
                    File videoFile = new File(playlist[i]);
                    sb.append(String.format("%d: %s\n", i, videoFile.getName()));
                }

                sender.sendMessage(sb.toString());

                return true;
            }
        }

        return false;
    }

    private boolean checkPermission(CommandSender sender, String permission) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            return player.hasPermission(permission);
        } else {
            return true;
        }
    }
}
